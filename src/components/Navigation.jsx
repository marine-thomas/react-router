import "./Navigation.css";
import { Outlet, NavLink } from "react-router-dom";

function Navigation() {
  return (
    <>
      
      <nav className="Navigation">
        {/* Choix de Navlink plutôt que Link pou pouvoir changer le style du lien lorsqu'il sera actif*/}
        <NavLink to={`/`}>Home</NavLink>
        <NavLink to={`/products`}>Products</NavLink>
        <NavLink to={`/cart`}>Cart</NavLink>
      </nav>
      <div>
        <Outlet /> { /* Permet d'ajouter ce composant navigation sur toutes les pages */ }
      </div>
    </>
  );
}

export default Navigation;
