import React from "react";
import ReactDOM from "react-dom/client";
import {
  createBrowserRouter,
  RouterProvider,
} from "react-router-dom";
import Navigation from "./components/Navigation";
import ErrorPage from "./error-page";

// TODO install and import react router

import Cart from "./pages/cart/Cart";
import Home from "./pages/home/Home";
import ProductList from "./pages/products/ProductList";
import "./index.css";

const router = createBrowserRouter([
  {
    path: "/", //On définit la route principale qui correspond à la racine
    element: <Navigation/> , //On affiche sur la route principale la navigation
    errorElement: <ErrorPage />, //La page d'erreur sera celle-ci
    children : [ //On définit ici les enfants
    {
      path: "/", //Cette route enfant correspond aussi à la racine
      element: <Home />, //On affiche sur la route principale le composant home
    },
    {
      path: "/cart", //Cette route enfant prendra le chemin suivant
      element: <Cart />,
    },
    {
      path: "/products", //Cette route enfant prendra le chemin suivant
      element: <ProductList />,
    }],
  }
]);

ReactDOM.createRoot(document.getElementById("root")).render(
  <React.StrictMode>
    {/* TODO use react router here */}
    <RouterProvider router={router} />
  </React.StrictMode>
);
